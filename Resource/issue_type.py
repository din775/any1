import Settings.JConnect as jira

class ProjectX:
    def __init__(self, project, issue):
        self.project=project
        self.issue = issue
        self.conn = jira.Connect('user', 'pass')

    def create(self):
        issue_description={
            'project': self.project,
            'customfield_10304': self.issue.phone,
            'summary': self.issue.summary,
            'description': self.issue.description,
            'priority': self.issue.priority,
            'issuetype': self.issue.type_issue
        }
        if (self.issue.type_issue and 'name' in self.issue.type_issue.keys() 
        and self.issue.type_issue.get('name') == 'Incidente'):
            issue_description.update({
                'customfield_10313': self.issue.cat_issue
            })
        if (self.issue.type_issue and 'name' in self.issue.type_issue.keys() 
        and self.issue.type_issue.get('name') == 'Solicitação'):
            issue_description.update({
                'customfield_10313': self.issue.cat_issue
            })

        issue = self.conn.create_issue(**issue_description)
        if issue:
            self.issue.key = issue.key
            return self.issue.key
        return False

    def assign(self, username):
        return self.conn.assign_issue(self.issue.key, username)
        

    def link(self, type_link, outissue):
        self.conn.create_issue_link(
        type_link= type_link,
        inwardIssue= self.issue.key,
        outwardIssue= outissue
        )


class IssueType:
    summary = None
    description = None
    phone = None
    attachment = None
    organization = None
    priority = None
    request_participants = None



class Incident(IssueType):
    resolution = None
    type_issue = {'name': 'Incidente'} 

    def __init__(self, summary, description, phone, priority, cat_issue,portal=False):
        self.summary = summary
        self.description = description
        self.phone = phone
        self.priority = priority
        self.portal = portal
        self.cat_issue=cat_issue
        self.key = None


class Gmud(IssueType):
    pass



class Solicitaion(IssueType):
    def __init__(self, summary, description, phone, priority, portal=False):
        self.summary = summary
        self.description = description
        self.phone = phone
        self.priority = priority
        self.portal = portal
        self.key = None

class GProblem(IssueType):
    pass
